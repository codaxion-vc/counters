`timescale 1ns/1ps

module tb_counter;

reg        clk    ;
wire [3:0] count    ;

counter uut (
    .clk    (    clk    ),
    .count    (    count    )
);

parameter PERIOD = 10;

initial
begin
    $dumpfile("db_tb_counter.vcd");
    $dumpvars(0, tb_counter);
    clk = 1'b0;
    #100 $finish;
end

always
begin
#2 clk = ~clk;
end

endmodule
